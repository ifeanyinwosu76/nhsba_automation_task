require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
class SpanTag


  def self.click(driver, locator, text_to_find)
    number_of_time = 1
    until number_of_time >= YamlReader.get_dev_data('default_time') do
      if(driver.find_element(locator).displayed?)
        account_elements = driver.find_element(locator).find_elements(:tag_name => 'span')
        account_elements.each do |account_element|
          if(account_element.text.downcase.include?(text_to_find.downcase))
            account_element.click
            return
          end
        end
      else
        sleep 1
        number_of_time =+ 1
      end
    end
  end




end