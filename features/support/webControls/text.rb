require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
class Text


  def self.get_text(driver, locator)
    times = 1
    until times >= YamlReader.get_dev_data('default_time')  do
      if(driver.find_element(locator).displayed?)
        return driver.find_element(locator).text
      else
        sleep(1)
        times += 1;
      end
    end
  end
end