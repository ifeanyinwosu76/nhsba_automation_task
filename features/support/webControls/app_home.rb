require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
class AppHome

  def self.visit_home(driver)
    url = YamlReader.get_dev_data('base_url')
    driver.navigate.to(url)
  end


  def self.visit(driver, url)
    driver.navigate.to(url)
  end


  def self.reload_page(driver)
    driver.navigate.refresh
  end

  def self.go_back(driver)
    driver.navigate.back
  end


  def self.go_forward(driver)
    driver.navigate.forward
  end


end