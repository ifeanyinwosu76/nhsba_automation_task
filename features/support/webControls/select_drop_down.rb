require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'

class SelectDropDown


  def self.select_drop_down_with_text(driver, locator, required_text_to_be_selected)
    number_of_times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until number_of_times >= default_time  do
      if(driver.find_element(locator).enabled?)
        taxOtion = Selenium::WebDriver::Support::Select.new(driver.find_element(locator))
        taxOtion.select_by(:text, required_text_to_be_selected)
        return
      else
        sleep(1)
        number_of_times =+ 1
      end
    end
  end


  def self.select_drop_down_with_value(driver, locator, required_value)
    number_of_times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until number_of_times >= default_time  do
      if(driver.find_element(locator).enabled?)
        taxOtion = Selenium::WebDriver::Support::Select.new(driver.find_element(locator))
        taxOtion.select_by(:value, required_value)
        return
      else
        sleep(1)
        number_of_times =+ 1
      end
    end
  end


  def self.select_drop_down_with_index(driver, locator, required_index)
    number_of_times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until number_of_times >= default_time  do
      if(driver.find_element(locator).enabled?)
        taxOtion = Selenium::WebDriver::Support::Select.new(driver.find_element(locator))
        taxOtion.select_by(:index, required_index)
        return
      else
        sleep(1)
        number_of_times =+ 1
      end
    end
  end







end