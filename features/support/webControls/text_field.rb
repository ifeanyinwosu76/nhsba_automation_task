require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
class TextField



  def self.enter_text(driver, locator, required_text)
    times = 1
    until times >= YamlReader.get_dev_data('default_time')  do
      if(driver.find_element(locator).enabled? && driver.find_element(locator).displayed?)
        driver.find_element(locator).send_keys(required_text.to_s)
        return
      else
        sleep(1)
        times += 1;
      end
    end
  end





end