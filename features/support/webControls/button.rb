require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
class Button



  def self.click_button_tag(driver, locator, attribute_name, button_name)
    number_of_times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until number_of_times >= default_time do
      if(driver.find_element(locator).displayed?)
        button_elements = driver.find_element(locator).find_elements(:tag_name => 'button')
        button_elements.each do |button_element|
          if(button_element.attribute(attribute_name).downcase.include?(button_name.downcase))
            button_element.click
            return
          end
        end
      else
        sleep(1)
        number_of_times += 1;
      end
    end
  end


  def self.click(driver, locator)
    times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until times >= default_time  do
      if(driver.find_element(locator).enabled? && driver.find_element(locator).displayed?)
        driver.find_element(locator).click
        break
      else
        sleep(1)
        times += 1;
      end
    end

  end






end



