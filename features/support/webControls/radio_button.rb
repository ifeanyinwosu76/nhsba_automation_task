require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'

class RadioButton



  def self.click_on_button(driver, locator)
    times = 1
    default_time = YamlReader.get_dev_data('default_time')
    until times >= default_time
      if(driver.find_element(locator).enabled?)
        driver.find_element(locator).click
        return
      else
        sleep(1)
        times = times + 1
      end
    end
  end


end