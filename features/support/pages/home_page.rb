require_relative '../pages/base_page'

class HomePage < BasePage

 START_LOCATOR = {:id => 'next-button'}
 WALES_LOCATOR = {:id => 'label-wales'} #radio_wales
 NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def proceed_as_wales
    Button.click(driver, START_LOCATOR)
    RadioButton.click_on_button(driver, WALES_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    BirthIdentityPage.new(driver)
  end

  def validate_logout(message)
    return driver.page_source.downcase.include?(message.downcase)
  end


end