require_relative '../pages/base_page'

class BenefitPage < BasePage


  NO_BENEFIT_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_benefit_detail
    RadioButton.click_on_button(driver, NO_BENEFIT_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    PregnancyPage.new(driver)
  end


end