require_relative '../pages/base_page'

class PartnerPage < BasePage


  NO_PARTNER_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_partner_detail
    RadioButton.click_on_button(driver, NO_PARTNER_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    BenefitPage.new(driver)
  end

end