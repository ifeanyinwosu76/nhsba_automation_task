require_relative '../pages/base_page'

class InjuryPage < BasePage


  NO_INJURY_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_injury_detail
    RadioButton.click_on_button(driver, NO_INJURY_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    DiabetesPage.new(driver)
  end

end