require_relative '../pages/base_page'

class ResultPage < BasePage

  FREE_PRESCRIPTION = {:id => 'prescription-tick'}

  def initialize(driver)
    super(driver)
  end

  def validate_free_prescription(prescription)
    Text.get_text(driver, FREE_PRESCRIPTION).include?(prescription)
  end
end