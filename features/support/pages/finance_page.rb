require_relative '../pages/base_page'

class FinancePage < BasePage


  NO_EARNING_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end



  def enter_financial_detail
    RadioButton.click_on_button(driver, NO_EARNING_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    ResultPage.new(driver)
  end


end