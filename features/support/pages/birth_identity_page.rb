require_relative '../pages/base_page'

class BirthIdentityPage < BasePage


  DAY_LOCATOR = {:id => 'dob-day'}
  MONTH_LOCATOR = {:id => 'dob-month'}
  YEAR_LOCATOR = {:id => 'dob-year'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_dob
    TextField.enter_text(driver, DAY_LOCATOR, '17')
    TextField.enter_text(driver, MONTH_LOCATOR, '07')
    TextField.enter_text(driver, YEAR_LOCATOR, '1960')
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    PartnerPage.new(driver)
  end



end