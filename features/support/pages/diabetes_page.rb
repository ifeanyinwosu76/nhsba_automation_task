require_relative '../pages/base_page'

class DiabetesPage < BasePage


  YES_DIABETES_LOCATOR = {:id => 'label-yes'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_diabetic_detail
    RadioButton.click_on_button(driver, YES_DIABETES_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    GlaucomaPage.new(driver)
  end



end