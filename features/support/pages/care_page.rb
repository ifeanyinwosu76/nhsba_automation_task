require_relative '../pages/base_page'

class CarePage < BasePage


  NOT_IN_CARE_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end



  def enter_care_detail
    RadioButton.click_on_button(driver, NOT_IN_CARE_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    FinancePage.new(driver)
  end

end