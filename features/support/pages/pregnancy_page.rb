require_relative '../pages/base_page'

class PregnancyPage < BasePage


  NO_PREGNANCY_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end


  def enter_pregnancy_detail
    RadioButton.click_on_button(driver, NO_PREGNANCY_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    InjuryPage.new(driver)
  end


end