require_relative '../../../Infrastructure/data_reader_engine/yaml_reader'
require_relative '../webControls/anchor_tag'
require_relative '../../../features/support/webControls/app_home'
class BasePage

  attr_accessor :driver
  @driver


  def initialize(driver)
    @driver = driver
  end


  def visit_home_page
    AppHome.visit_home(driver)
    AppHome.reload_page(driver)
    HomePage.new(driver)
  end













end