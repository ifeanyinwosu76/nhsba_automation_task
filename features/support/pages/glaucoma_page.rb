require_relative '../pages/base_page'

class GlaucomaPage < BasePage


  NO_GLAUCOMA_LOCATOR = {:id => 'label-no'}
  NEXT_BUTTON_LOCATOR = {:id => 'next-button'}


  def initialize(driver)
    super(driver)
  end



  def enter_glaucoma_detail
    RadioButton.click_on_button(driver, NO_GLAUCOMA_LOCATOR)
    Button.click(driver, NEXT_BUTTON_LOCATOR)
    CarePage.new(driver)
  end

end