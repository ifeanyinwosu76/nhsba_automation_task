Given(/^I am a person from Wales$/) do
  @birthIdentityPage = @basePage.visit_home_page.proceed_as_wales
end

When(/^I put my circumstances into the Checker tool$/) do
  @resultPage = @birthIdentityPage
      .enter_dob
      .enter_partner_detail
      .enter_benefit_detail
      .enter_pregnancy_detail
      .enter_injury_detail
      .enter_diabetic_detail
      .enter_glaucoma_detail
      .enter_care_detail
      .enter_financial_detail
end

Then(/^I should get a result of whether I will get help or not$/) do
  expect(@resultPage.validate_free_prescription("NHS prescriptions")).to be true
end