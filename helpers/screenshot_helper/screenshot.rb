class Screenshot


  def grab_screen_shot(scenario, driver)
    if scenario.respond_to?('scenario_outline') then
      scenario = scenario.scenario_outline
    end
    if(scenario.failed?)

      # encoded_img = driver.screenshot_as(:base64)
      # embed("data:image/png;base64,#{encoded_img}",'image/png')

      filename = "./test_reports/screenshots/error-screen-#{Time.now}.png"
      driver.save_screenshot(filename)
      embed(filename, 'image/png') 
    end
  end




end