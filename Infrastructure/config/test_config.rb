class TestConfig

  ENV['CSV_PATH'] =  File.join(File.expand_path('..'),'/testdata/')
  ENV['PROJECT_PATH'] =File.join(File.expand_path('..'),'/NhsCostsCheckerTest/')
  ENV['DRIVER_PATH'] = File.join(File.expand_path('..'),'NhsCostsCheckerTest/drivers/')

end